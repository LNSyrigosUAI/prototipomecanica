﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class ControlJugador_Tirador : MonoBehaviour
{
    //************VARIABLES************
    //MOVIMIENTO.
    public float VelocidadCaminar = 5f;
    public float VelocidadCorrer = 10f;
    Vector3 InputMovimiento = Vector3.zero;//Vector3.zero inicializa en 0 cada uno de los parametros XYZ.
    CharacterController characterController;
    //SALTO.
    public float AlturaSalto = 1.9f;
    public float EscalaGravedad = -20f;
    //CAMARA.
    public Camera CamaraJugador;
    Vector3 InputRotacion = Vector3.zero;
    public float SensibilidadRotacion = 10f;
    private float AnguloVerticalCamara;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        Mirar();
        Movimiento();
    }

    private void Movimiento()
    {
        if (characterController.isGrounded)
        {
            InputMovimiento = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
            InputMovimiento = Vector3.ClampMagnitude(InputMovimiento, 1f);
            if (Input.GetButton("Sprint"))
            {
                InputMovimiento = transform.TransformDirection(InputMovimiento) * VelocidadCorrer;
            }
            else
            {
                InputMovimiento = transform.TransformDirection(InputMovimiento) * VelocidadCaminar;
            }
            if (Input.GetButtonDown("Jump"))
            {
                InputMovimiento.y = Mathf.Sqrt(AlturaSalto * -2f * EscalaGravedad);
            }
        }
        InputMovimiento.y += EscalaGravedad * Time.deltaTime;
        characterController.Move(InputMovimiento * Time.deltaTime);
    }

    private void Mirar()
    {
        InputRotacion.x = Input.GetAxis("Mouse X") * SensibilidadRotacion * Time.deltaTime;
        InputRotacion.y = Input.GetAxis("Mouse Y") * SensibilidadRotacion * Time.deltaTime;
        AnguloVerticalCamara = AnguloVerticalCamara + InputRotacion.y;
        AnguloVerticalCamara = Mathf.Clamp(AnguloVerticalCamara, -70, 70);
        transform.Rotate(Vector3.up * InputRotacion.x);
        CamaraJugador.transform.localRotation = Quaternion.Euler(-AnguloVerticalCamara, 0f, 0f);
    }
}
