﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador_Corredor : MonoBehaviour
{
    //**********VARIABLES**********.
    //MOVIMIENTO DEL JUGADOR.
    public float MovimientoHorizontal;
    public float MovimientoVertical;
    public CharacterController Jugador;
    [Range(0f,100f)] public float VelocidadJugador;
    private Vector3 InputJugador;
    private Vector3 MovimientoJugador;
    //GRAVEDAD.
    [Range(0f,100f)] public float Gravedad = 9.8f;
    [Range(0f,100f)] public float VelocidadCaida;
    //SALTO.
    [Range(0f,100f)] public float FuerzaSalto;
    //CAMARA.
    public Camera Camara;
    private Vector3 CamaraAdelante;
    private Vector3 CamaraDerecha;


    void Start()
    {
        Jugador = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        MovimientoHorizontal = Input.GetAxis("Horizontal 1");
        MovimientoVertical = Input.GetAxis("Vertical 1");
        InputJugador = new Vector3(MovimientoHorizontal, 0, MovimientoVertical);
        InputJugador = Vector3.ClampMagnitude(InputJugador, 1);
        DireccionDeCamara();
        MovimientoJugador = InputJugador.x * CamaraDerecha + InputJugador.z * CamaraAdelante;
        MovimientoJugador = MovimientoJugador * VelocidadJugador;
        Jugador.transform.LookAt(Jugador.transform.position + MovimientoJugador);
        EstablecerGravedad();
        HabilidadesJugador();
        Jugador.Move(MovimientoJugador * Time.deltaTime);
    }

    public void DireccionDeCamara()
    {
        CamaraAdelante = Camara.transform.forward;
        CamaraDerecha = Camara.transform.right;
        CamaraAdelante.y = 0;
        CamaraDerecha.y = 0;
        CamaraAdelante = CamaraAdelante.normalized;
        CamaraDerecha = CamaraDerecha.normalized;
    }

    public void HabilidadesJugador()
    {
        if (Jugador.isGrounded && Input.GetButtonDown("Jump 1"))
        {
            VelocidadCaida = FuerzaSalto;
            MovimientoJugador.y = VelocidadCaida;
        }
    }

    public void EstablecerGravedad()
    {
        if (Jugador.isGrounded)
        {
            VelocidadCaida = -Gravedad * Time.deltaTime;//VELOCIDAD DE CAIDA.
            MovimientoJugador.y = VelocidadCaida;
        }
        else
        {
            VelocidadCaida -= Gravedad * Time.deltaTime;//RESTA LA GRAVEDAD, PARA GENERAR UNA VELOCIDAD DE CAIDA.
            MovimientoJugador.y = VelocidadCaida;
        }
    }

    /*
     * private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    [SerializeField] private float playerSpeed = 2.0f;
    [SerializeField] private float jumpHeight = 1.0f;
    [SerializeField] private float gravityValue = -9.81f;

    private void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
    }

    void Update()
    {
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        // Changes the height position of the player..
        if (Input.GetButtonDown("Jump") && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }*/
}
