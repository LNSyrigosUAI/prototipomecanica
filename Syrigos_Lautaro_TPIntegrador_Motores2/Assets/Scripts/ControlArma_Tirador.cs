﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlArma_Tirador : MonoBehaviour
{
    //ARMAS INICIALES.
    public List<ControlArma> ArmasIniciales = new List<ControlArma>();
    //CONEXIONES DONDE APARACEN LAS ARMAS.
    public Transform PosicionArmaPrinicipal;
    public Transform PosicionDeterminadaArma;
    public Transform PosicionAim;
    public int IndiceArmaActiva { get; private set; }//PARA AVISAR CUAL ES EL ARMA ACTIVA QUE TIENE EL JUGADOR.
    private ControlArma[] EspaciosArmas = new ControlArma[5];//EL MAXIMO DE ARMAS QUE PUEDE TENER EL TIRADOR DE FORMA FIJA, POR ESO SE UTILIZA EL ARRAY.


    void Start()
    {
        IndiceArmaActiva = -1;
        foreach(ControlArma _ArmaInicial in ArmasIniciales)
        {
            AgregarArma(_ArmaInicial);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            CambiarArma(0);
        }
    }

    private void CambiarArma(int _IndiceArma)
    {
        if (_IndiceArma != IndiceArmaActiva && _IndiceArma >= 0)
        {
            EspaciosArmas[_IndiceArma].gameObject.SetActive(true);
            IndiceArmaActiva = _IndiceArma;
            ControlEventos.Actual.NuevaArmaEvento.Invoke();
        }
    }

    private void AgregarArma(ControlArma _ArmaPrefab)
    {
        PosicionArmaPrinicipal.position = PosicionDeterminadaArma.position;
        //PosicionDeterminadaArma.position = PosicionArmaPrinicipal.position;
        for(int i = 0; i < EspaciosArmas.Length; i++)//AGREGA EL ARMA PERO NO LA MUESTRA.
        {
            if (EspaciosArmas[i] == null)//VERIFICA SI HAY ESPACIOS VACIO Y AGREGA EL ARMA.
            {
                ControlArma ArmaClonada = Instantiate(_ArmaPrefab, PosicionArmaPrinicipal);
                ArmaClonada.Propietario = gameObject;//PARA QUE EL ARMA IDENTIFIQUE AL JUGADOR COMO USUARIO.
                ControlEventos.Actual.NuevaArmaEvento.Invoke();
                ArmaClonada.gameObject.SetActive(false);//SE AGREGA AL JUGADOR PERO NO SE VE.
                //RETORNA SI HAY ESPACIOS PARA AGREGAR EL ARMA.
                EspaciosArmas[i]= ArmaClonada;
                return;
            }
        }
    }
}