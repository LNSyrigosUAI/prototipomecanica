﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[SerializeField]
public class IcontadorMunicion: UnityEvent<int, int> { }
public class ControlEventos : MonoBehaviour
{
    #region SINGLETON
    public static ControlEventos Actual;
    private void Awake()
    {
        if (Actual == null) { Actual = this; } else if (Actual != null) { Destroy(this); }
    }
    #endregion

    public IcontadorMunicion MunicionEvento = new IcontadorMunicion();
    public UnityEvent NuevaArmaEvento = new UnityEvent();
}
