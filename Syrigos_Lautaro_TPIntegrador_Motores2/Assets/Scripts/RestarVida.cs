﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestarVida : MonoBehaviour
{
    VidaJugador JugadorVida;
    public int Cantidad;
    public float TiempoDaño;
    private float TiempoDañoActual;

    void Start()
    {
        JugadorVida = GameObject.FindWithTag("Runner").GetComponent<VidaJugador>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Runner")
        {
            TiempoDañoActual += Time.deltaTime;
            if (TiempoDañoActual > TiempoDaño)
            {
                JugadorVida.Vida += Cantidad;
                TiempoDañoActual = 0.0f;
            }
        }
    }

}
