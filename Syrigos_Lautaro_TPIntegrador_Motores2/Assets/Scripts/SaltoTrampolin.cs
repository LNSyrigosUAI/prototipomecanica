﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoTrampolin : MonoBehaviour
{
    public float Jumpv = 0.4f;
    public Rigidbody rb;
    public float thrust = 10;
    bool isGrounded;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        isGrounded = true;
    }


    void Update()
    {
        if (Input.GetKeyDown("Jump 1") && isGrounded == true)
        {
            Jump();
        }
    }

    public void Jump()
    {
        isGrounded = false;
        rb.AddForce(0, thrust, 0, ForceMode.Impulse);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Trampolin"))
        {
            isGrounded = true;
        }
    }

}
