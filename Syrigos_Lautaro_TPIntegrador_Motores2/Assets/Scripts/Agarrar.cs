﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agarrar : MonoBehaviour
{
    public GameObject ObjetoAgarrable;
    public GameObject ObjetoAgarrado;
    public Transform Manos;

    void Update()
    {
        if (ObjetoAgarrable != null && ObjetoAgarrable.GetComponent<Agarrable>().esAgarrable == true && ObjetoAgarrado == null)
        {
            if (Input.GetKeyDown("Agarrar"))
            {
                ObjetoAgarrado = ObjetoAgarrable;
                ObjetoAgarrado.GetComponent<Agarrable>().esAgarrable = false;
                ObjetoAgarrado.transform.SetParent(Manos);
                ObjetoAgarrado.transform.position = Manos.position;
                ObjetoAgarrado.GetComponent<Rigidbody>().useGravity = false;
                ObjetoAgarrado.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
        else if (ObjetoAgarrado != null)
        {
            if (Input.GetKeyDown("Agarrar"))
            {
                ObjetoAgarrado.GetComponent<Agarrable>().esAgarrable = true;
                ObjetoAgarrado.transform.SetParent(null);
                ObjetoAgarrado.GetComponent<Rigidbody>().useGravity = true;
                ObjetoAgarrado.GetComponent<Rigidbody>().isKinematic = false;
                ObjetoAgarrado = null;
            }
        }
    }

}
