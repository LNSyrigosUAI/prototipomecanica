﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ControlArma_UI : MonoBehaviour
{
    public TMP_Text MunicionActual;
    public TMP_Text MunicionMaxima;

    private void OnEnable()
    {
        ControlEventos.Actual.MunicionEvento.AddListener(ActualizarMunicion);
    }

    private void OnDisable()
    {
        ControlEventos.Actual.MunicionEvento.RemoveListener(ActualizarMunicion);
    }

    public void ActualizarMunicion(int _nuevaMunicionActual, int _nuevaMunicionMaxima)
    {
        if (_nuevaMunicionActual <= 0)
        {
            MunicionActual.color = new Color(1,0,0);
        }
        else
        {
            MunicionActual.color = Color.white;
        }
        MunicionActual.text = _nuevaMunicionActual.ToString();
        MunicionMaxima.text = _nuevaMunicionMaxima.ToString();
    }
}