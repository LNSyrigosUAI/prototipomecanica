﻿using System.Collections;
using UnityEngine;

public class ControlArma : MonoBehaviour
{
    [Range(0f, 1000)] public float RangoDisparo;
    public LayerMask CapaObjetivo;
    private Transform JugadorCamara;
    public GameObject AgujeroBala;
    public float RetrocesoFuerza = 4f;
    public Transform CañonArma;
    public GameObject FlashDisparo;
    public float Cadencia = 0.6f;
    public int MunicionMaxima = 8;
    private float _UltimoDisparo = Mathf.NegativeInfinity;
    public int MunicionActual { get; private set; }
    public float TiempoRecarga = 1.5f;
    public GameObject Propietario { set; get; }

    private void Awake()
    {
        MunicionActual = MunicionMaxima;
        ControlEventos.Actual.MunicionEvento.Invoke(MunicionActual, MunicionMaxima);
    }

    private void Start()
    {
        JugadorCamara = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            ProbarDisparo();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(Recargar());
        }

        transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, Time.deltaTime * 5f);//PARA QUE EL ARMA VUELVA A LA POSICION INICIAL.
    }

    private bool ProbarDisparo()
    {
        if (_UltimoDisparo + Cadencia < Time.time)
        {
            if (MunicionActual >= 1)
            {
                Disparar();
                MunicionActual -= 1;
                ControlEventos.Actual.MunicionEvento.Invoke(MunicionActual, MunicionMaxima);
                return true;
            }
        }
        return false;
    }

    private void Disparar()
    {
        GameObject FlashClone = Instantiate(FlashDisparo, CañonArma.position, Quaternion.Euler(CañonArma.forward), transform);
        Destroy(FlashClone, 1f);
        AgregarRetroceso();
        RaycastHit[] hits;
        hits = Physics.RaycastAll(JugadorCamara.position, JugadorCamara.forward, RangoDisparo, CapaObjetivo);
        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.gameObject != Propietario)
            {
                GameObject _AgujeroBalaClone = Instantiate(AgujeroBala, hit.point + hit.normal * 0.001f, Quaternion.LookRotation(hit.normal));
                Destroy(_AgujeroBalaClone, 6f);
            }
        }
        _UltimoDisparo = Time.time;//TIME.TIME = REGRESA AL TIEMPO ACTUAL DE JUEGO.
    }

    private void AgregarRetroceso()
    {
        transform.Rotate(-RetrocesoFuerza, 0f, 0f);
        transform.position = transform.position - transform.forward * (RetrocesoFuerza / 50f);
    }

    IEnumerator Recargar()
    {
        Debug.Log("Estoy Recargando...");
        yield return new WaitForSecondsRealtime(TiempoRecarga);
        MunicionActual = MunicionMaxima;
        ControlEventos.Actual.MunicionEvento.Invoke(MunicionActual, MunicionMaxima);
        Debug.Log("Esta lista");
    }
}
