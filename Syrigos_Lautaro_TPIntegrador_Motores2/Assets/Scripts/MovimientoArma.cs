﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoArma : MonoBehaviour
{
    private Quaternion RotacionOriginal;//PARA GUARDAR LA ROTACION DEL OBJETO.

    private void Start()
    {
        RotacionOriginal = transform.localRotation;//LA VARIABLE ES IGUAL A LA ROTACION DEL ARMA.
    }

    void Update()
    {
        ActualizarMovimiento();
    }

    private void ActualizarMovimiento()
    {
        //INPUT DE MOVIMIENTO.
        float InputMirar_X = Input.GetAxis("Mouse X");
        float InputMirar_Y = Input.GetAxis("Mouse Y");
        //CALCULAR LA ROTACION DEL ARMA.
        Quaternion Angulo_X = Quaternion.AngleAxis(-InputMirar_X * 1.45f, Vector3.up);
        Quaternion Angulo_Y = Quaternion.AngleAxis(InputMirar_Y * 1.45f, Vector3.right);
        Quaternion RotacionObjetivo = RotacionOriginal * Angulo_X * Angulo_Y;
        //GIRAR HACIA LA ROTACION DEL OBJETIVO.
        transform.localRotation = Quaternion.Lerp(transform.localRotation, RotacionObjetivo, Time.deltaTime * 10f);
    }
}