﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara_Corredor : MonoBehaviour
{
    public Vector3 Offset;//DISTANCIA ENTRE LA CAMARA Y EL JUGADOR.
    private Transform _Etiqueta;
    [Range(0, 1)] public float LerpValue;//SUAVIDAD DE LA CAMARA.
    [Range(0, 100)] public float Sensibilidad;

    void Start()
    {
        _Etiqueta = GameObject.Find("Corredor").transform;
    }

    void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, _Etiqueta.position + Offset, LerpValue);
        Offset = Quaternion.AngleAxis(Input.GetAxis("Stick") * Sensibilidad, Vector3.up) * Offset;
        transform.LookAt(_Etiqueta);
    }
}
