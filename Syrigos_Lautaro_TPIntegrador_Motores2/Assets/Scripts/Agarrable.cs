﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agarrable : MonoBehaviour
{
    public bool esAgarrable = true;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ZonaInteraccion")
        {
            other.GetComponentInParent<Agarrar>().ObjetoAgarrable = this.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ZonaInteraccion")
        {
            other.GetComponentInParent<Agarrar>().ObjetoAgarrable = null;
        }
    }

}
