﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaJugador : MonoBehaviour
{
    public float Vida = 100;
    public Image BarraDeVida;
    void Update()
    {
        Vida = Mathf.Clamp(Vida, 0, 100);
        BarraDeVida.fillAmount = Vida / 100;
    }
}
