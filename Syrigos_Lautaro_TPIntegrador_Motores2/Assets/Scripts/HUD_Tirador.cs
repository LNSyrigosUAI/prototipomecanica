﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD_Tirador : MonoBehaviour
{
    public GameObject informacionArma;

    private void Start()
    {
        ControlEventos.Actual.NuevaArmaEvento.AddListener(CrearInformacionArma);
    }

    public void CrearInformacionArma()
    {
        Instantiate(informacionArma, transform);
    }
}
